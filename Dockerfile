FROM ubuntu

MAINTAINER Robert Sitko

RUN apt-get update \
  && apt-get -y upgrade \
  && apt-get install -y build-essential \ 
  git-all \
  wget \
  unzip \
  apt-utils \
  cmake \
  gfortran \
  libopenblas-dev \
  liblapack-dev \
  libpng-dev \
  libfreetype6-dev \
  python-dev \
  python-pip \
  python-tk \
  python-pyopencl \
  && cd \
          && wget https://github.com/opencv/opencv/archive/3.1.0.zip \
          && unzip 3.1.0.zip \
          && cd opencv-3.1.0 \
          && mkdir build \
          && cd build \
          && cmake .. \
          && make -j4 \
          && make install \
          && cd \
  && rm 3.1.0.zip \
  && git clone https://bitbucket.org/maciekwielgosz/htm-hardware-architecture.git \
  && cd htm-hardware-architecture \
  && pip install --upgrade pip \
  && pip install -r requirements.txt; easy_install numpy; pip install -r requirements.txt \
  && python setup.py develop \
  && apt-get install -y graphviz \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get update