Installation:  
1. [Install Docker](https://docs.docker.com/engine/installation/)  
2. `git clone https://rsitko@bitbucket.org/rsitko/htm-docker.git`  
3. `cd htm-docker`  
4. `sudo service docker start`  
5. `sudo docker build -t htm-docker - < Dockerfile`  
  
Running container:  
1. `sudo docker run -it htm-docker /bin/bash`  
HTM project is in /root/htm-hardware-architecture (`cd /root/htm-hardware-architecture`).  
Image htm-docker hasn't OpenCL drivers so use CPU implementation of project.  
  
Deleting all Docker's images and containers on host machine:  
1. `chmod +x ./delete.sh`  
2. `./delete.sh`  